# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    my_gear = []
    if is_workday == True:
        my_gear.append("laptop")
    if is_workday == False:
        my_gear.append("Surfboard")
    if is_sunny == False and is_workday == True:
        my_gear.append("Umbrella")
    return my_gear


sunny = False
working = True

print("Bring this for today:" , gear_for_day(working, sunny))

#A bunch of conditionals
#if working, add laptop
#if not working, add surfboard
#if not sunny and working, add umbrella




#DONE
