# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    reversed = word[::-1]
    if word == reversed:
        return word + " is a palindrome"
    else:
        return word + " is not a palindrome"

print(is_palindrome("poop"))
print(is_palindrome("cats"))

#set word to variable
#reverse the variable
#compare to see if the new var is the same as the input

#DONE
