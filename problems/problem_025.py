# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if len(values) == 0:
        return None
    else:
        sum = 0
        for number in values:
            sum += number
        return sum


values1 = [3,6,4,7,43]
values2 = []
values3 = [0,0,0]
values4 = [-3, -99, 2990, -9273972]

print(calculate_sum(values1))
print(calculate_sum(values2))
print(calculate_sum(values3))
print(calculate_sum(values4))



#DONE


#INPUT: LIST OF INTS
#OUTPUT: SUM OF INTS, IF EMPTY RETURN NONE
