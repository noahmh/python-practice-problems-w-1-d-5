# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    new_string = ""
    repeats= 0
    if len(s) == 0:
        return None
    else:
        for letter in s:
            if new_string.find(letter) < 0:
                new_string += letter
        return new_string


mystring = "abccba"
print(remove_duplicate_letters(mystring))

#INPUT: STRING
#OUTPUT: STRING,


#DONE




# loop thru the word
# for each char in word
#     add to a new string if char is not in string
