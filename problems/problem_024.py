# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    total = 0
    if len(values) == 0:
        return None
    else:
        for number in values:
            total += number
        average = total / len(values)
        return average

value_list1 = [38, 30, 27, 39, 22]
value_list2 = []
value_list3 = [0,0,0,0]

print(calculate_average(value_list1))
print(calculate_average(value_list2))
print(calculate_average(value_list3))



#DONE


#INPUT: list of ints
#OUTPUT: average of numbers, if empty return None

# make a for loop and add all the numbers
# divide sum by len of striing
# if len = 0, return None
