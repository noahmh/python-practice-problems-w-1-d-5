# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_numbers(limit):
    total = 0
    if limit < 0:
        return None
    else:
        my_range = range(limit + 1)
        for number in my_range:
            total += number
        return total

my_num = 3
my_num2 = 5
my_num3 = -1

print(sum_of_first_n_numbers(my_num))
print(sum_of_first_n_numbers(my_num2))
print(sum_of_first_n_numbers(my_num3))


#DONE




#INPUT: INT
#OUTPUT: INT

# take number and make a range from it, +1 to include
# the value of the number

# loop thru the range and add running total

# return total
