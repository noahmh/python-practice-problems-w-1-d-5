# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    largest = 0
    second_largest = 0
    if len(values) < 2:
        return None
    else:
        for number in values:
            if number == largest:
                largest = number
            elif number > largest:
                second_largest = largest
                largest = number
            elif number < largest and number >= second_largest:
                second_largest = number
        return second_largest


my_values = [1,2,100,100,5,6,99,3028, 3028]
my_values2 = [1]

print(find_second_largest(my_values))
print(find_second_largest(my_values2))
#make var for largest
#make var for 2 largest
# loop thru the list
# set current value to largest
# set value to sec if smaller than largest, and bigger than secon


#DONE
