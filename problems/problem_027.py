# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    max = 0
    if len(values) == 0:
        return None
    else:
        for number in values:
            if number > max:
                max = number
        return max


my_values = [1,5,6,20]
my_values2 = []

print(max_in_list(my_values))
print(max_in_list(my_values2))


#DONE

# loop thru the list and set the current number to the max if it is larger than current max
# check len of list and return empty
