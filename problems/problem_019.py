# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    x_to_rect = False
    y_to_rect = False
    x_between = False
    y_between = False
    if x >= rect_x:
        x_to_rect = True
    if y >= rect_y:
        y_to_rect = True
    if x <= rect_x + rect_width:
        x_between = True
    if y <= rect_y + rect_height:
        y_between = True
    if x_to_rect == True and y_to_rect == True and x_between == True and y_between == True:
        return "This input works"
    else:
        return "BAD INPUT"

x = 5
y = 6
rect_x = 1
rect_y = 2
rect_width = 20
rect_height = 30

print(is_inside_bounds(x,y,rect_x, rect_y, rect_width, rect_height))


#DONE
