# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    if "eggs" in ingredients and "oil" in ingredients and "flour" in ingredients:
        return True
    else:
        return False

my_pantry1 = ["eggs", "oil", "flour", "tomato"]
my_pantry2 = ["socks", "dresser", "oil"]

print(can_make_pasta(my_pantry1))
print(can_make_pasta(my_pantry2))



#input list of strings
#output true if flour eggs and oil are on list



#DONE
