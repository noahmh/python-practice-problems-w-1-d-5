# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.


def is_inside_bounds(x, y):
    x_valid = False
    y_valid = False
    if x >= 0 and x <= 10:
        x_valid = True
    if y >= 0 and y <= 10:
        y_valid = True
    if x_valid == True and y_valid == True:
        return "This is a valid input"
    else:
        return "This is not a valid input"

my_x1 = 4
my_x2 = 20

my_y1 = 5
my_y2 = 21

print(is_inside_bounds(my_x1, my_y1), ": this should be true")
print(is_inside_bounds(my_x2, my_y1), ": this should be false")
print(is_inside_bounds(my_x1, my_y2), ": this should be false")
print(is_inside_bounds(my_x2, my_y2), ": this should be false")

#if x >= 0 and <= 10
#if y >= 0 and <= 10
#return true
#return



#DONE
