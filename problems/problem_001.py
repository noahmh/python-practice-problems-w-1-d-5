# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    if value1 < value2:
        return value1
    elif value1 > value2:
        return value2
    else:
        return value1

val1 = 10
val2 = 10
my_comp = minimum_value(val1, val2)

print(my_comp)

# take the two values and compare them
# report the smaller of the two values
# if the values are the same, return both values



#DONE
